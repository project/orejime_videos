/* global orejime */
(function ($) {
  $(document).ready(function () {
    /**
     * Check if the social media cookie is enabled, and load content if enabled
     */
    function tryToLoadContent() {
      drupalSettings.orejime_videos.consents.forEach(function (consent) {
        const consentAccepted = orejime.internals.manager.getConsent(consent);
        if (consentAccepted) {
          $(`.orejime_template[data-orejime-consent="${consent}"]`).each(
            function () {
              const embededID = $(this).data("orejime-embeded-id");
              const content = $(this).html();

              $(
                `.orejime_placeholder[data-orejime-embeded-id='${embededID}'] .orejime_embed`
              ).replaceWith(content);
            }
          );
        }
      });
    }

    /**
     * Change the width of the placeholder based on the width-attribute
     */
    function changeWidthOfPlacholder() {
      $(".orejime_placeholder").each(function () {
        const width = $(this).attr("width");
        $(this).width(width);
      });
    }

    changeWidthOfPlacholder();
    tryToLoadContent();

    $(".orejime-loadconsent").on("click", function (e) {
      e.preventDefault();
      orejime.show();
    });

    $(document).on("click", ".orejime-Button--save", function () {
      tryToLoadContent();
    });
  });
})(jQuery);
