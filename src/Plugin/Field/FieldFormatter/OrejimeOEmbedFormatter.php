<?php

namespace Drupal\orejime_videos\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\media\Plugin\Field\FieldFormatter\OEmbedFormatter;

/**
 * Plugin implementation of the 'oembed' formatter.
 *
 * @internal
 *   This is an internal part of the oEmbed system and should only be used by
 *   oEmbed-related code in Drupal core.
 *
 * @FieldFormatter(
 *   id = "orejime_oembed",
 *   label = @Translation("Orejime oEmbed content"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class OrejimeOEmbedFormatter extends OEmbedFormatter {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = parent::viewElements($items, $langcode);

    // Get parent.
    $parent = $items->getParent()->getEntity();
    // Generate id based on parent entity type and entity id.
    $parentID = $parent->getEntityTypeId() . '-' . $parent->id();

    foreach ($elements as $key => &$element) {
      if ($this->needWrapper($items[$key])) {
        $filterSettings = $this->getServiceSettings($this->getService($items[$key]));

        $element = [
          '#theme' => 'orejime_video',
          '#original' => $element,
          '#contentID' => $parentID . '--' . $key,
          '#attributes' => [
            'width' => $element["#attributes"]["width"],
            'height' => $element["#attributes"]["height"],
          ],
          '#service' => $filterSettings['service'],
          '#consent' => $filterSettings["orejime_consent"],
          '#external_url' => $this->getExternalUrl($items[$key]),
        ];
      }
    }

    return $elements;
  }

  /**
   * Check if item need to be wrapped.
   *
   * @param $item
   *   The item to check.
   *
   * @return bool
   *   Return if item need to be wrapped.
   */
  protected function needWrapper($item): bool {
    if ($this->getService($item) !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get the service for the item.
   *
   * @param \Drupal\Core\Field\Plugin\Field\FieldType\StringItem $item
   *   The item to check.
   *
   * @return string|bool
   *   Return the service string or FALSE if there is none.
   */
  protected function getService(StringItem $item) {

    $value = $item->getValue();

    if (isset($value['value'])) {
      $url = UrlHelper::parse($value['value']);

      foreach (orejime_videos_filtered_domains() as $filteredService => $filterSettings) {
        foreach ($filterSettings['domains'] as $domain) {
          if (stripos($url['path'], $domain) !== FALSE) {
            return $filteredService;
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * Get settings of the service.
   *
   * @param string $service
   *   Name of the service.
   *
   * @return array
   *   Settings of the service.
   */
  protected function getServiceSettings(string $service): array {
    return orejime_videos_filtered_domains()[$service] + ['service' => $service];
  }

  /**
   *
   */
  protected function getExternalUrl($item) {
    $value = $item->getValue();

    if (isset($value['value'])) {
      return $value['value'];
    }

    return '';
  }

}
