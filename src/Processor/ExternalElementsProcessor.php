<?php

namespace Drupal\orejime_videos\Processor;

use DOMElement;
use DOMNode;
use DOMXPath;
use Drupal;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Core\Template\Attribute;

/**
 * Class to process.
 */
class ExternalElementsProcessor {

  /**
   * @param $text
   *
   * @return string
   */
  public function process($text): string {

    $dom = Html::load($text);
    $xpath = new DOMXPath($dom);
    $renderer = Drupal::service('renderer');
    $filtered_domains = orejime_videos_filtered_domains();

    $queries = [];

    foreach ($filtered_domains as $settings) {
      /*
       * Selector select:
       *  - elements with SRC attribute that contain the website domain name,
       *    that are not <source> and that do not have <picture> for parent
       *    ( <picture> can embed <img> )
       *  - <video> and <audio> elements that have a children with
       *    an SRC attribute
       *
       *  Tip: use http://xpather.com to test selector
       */
      foreach ($settings['domains'] as $domain) {
        $queries[] = "//*[(not(self::source) and contains(@src,\"{$domain}\") and not(parent::picture)) or ((self::video or self::audio or self::picture) and .//*[contains(@src, \"{$domain}\")])]";
      }
    }

    foreach ($xpath->query(implode('|', $queries)) as $domNode) {
      /** @var \DOMElement $domNode */
      $attributes = new Attribute();
      if ($domNode->hasAttribute('width')) {
        $attributes->setAttribute('width', $domNode->getAttribute('width'));
      }
      if ($domNode->hasAttribute('height')) {
        $attributes->setAttribute('height', $domNode->getAttribute('height'));
      }

      $scr = $domNode->getAttribute('src');
      $service = '';
      $consent = '';

      foreach ($filtered_domains as $filteredWebsite => $filteredSettings) {
        $service = $filteredWebsite;
        $consent = $filteredSettings["orejime_consent"];
        foreach ($filteredSettings['domains'] as $domain) {
          if (stripos($scr, $domain)) {
            $externalUrl = '';
            if (is_array($filteredSettings['htmlToExtUrl']) || !empty($filteredSettings['htmlToExtUrl'])) {
              $externalUrl = $this->domElementToUrl($domNode, $filteredSettings['htmlToExtUrl']);
            }
            break 2;
          }
        }
      }

      $html = $dom->saveHTML($domNode);

      $element = [
        '#theme' => 'orejime_video',
        '#original' => $html,
        '#contentID' => $this->getContentKey($html),
        '#attributes' => $attributes,
        '#service' => $service,
        '#consent' => $consent,
        '#external_url' => $externalUrl,
      ];

      $this->setInnerHtml($domNode, $renderer->render($element));
    }

    return Html::serialize($dom);
  }

  /**
   * Get the url of the service media based on DOMElements.
   *
   * @param \DOMElement $domNode
   *   The DomElement container the embed.
   * @param array $htmlExtractors
   *   The list of extractors.
   *
   * @return string
   *   The url of the element on the service.
   */
  private function domElementToUrl(\DOMElement $domNode, array $htmlExtractors) {
    $html = $domNode->ownerDocument->saveHTML($domNode);
    $url = '';

    foreach ($htmlExtractors as $urlExtractor) {
      preg_match($urlExtractor['pattern'], $html, $matches);
      if (!empty($matches[1])) {
        $url = str_ireplace('$1', $matches[1], $urlExtractor['replacement']);
        break;
      }
    }

    return $url;
  }

  /**
   * Get content key.
   *
   * @param string $content
   *   HTML content to crypt to get a base64 key.
   *
   * @return string
   */
  private function getContentKey($content) {
    return Crypt::hashBase64($content);
  }

  /**
   * @param \DOMElement $originalElement
   * @param $html
   */
  private function setInnerHtml($originalElement, $html) {
    $tmpDOM = Html::load($html);
    $tmpNode = $tmpDOM->getElementsByTagName('body')->item(0);
    /** @var \DOMDocument $document */
    $document = $originalElement->ownerDocument;
    $fragment = $document->createDocumentFragment();

    /** @var DOMElement $element */
    $element = $tmpNode->firstChild;
    do {
      $importedElement = $document->importNode($element, TRUE);
      $fragment->appendChild($importedElement);
    } while ($element = $element->nextSibling);

    $originalElement->parentNode->replaceChild($fragment, $originalElement);

  }

}
