# Orejime Complient Vidéos

Module replace embeded vidéos into placeholder with custom message and urls.

The module provide a **text filter** and a **oEmbed widget for media.**

The embeded vidéos content are placed in a `<template>` element and a custom html element is placed in place of the original element.
When the user accept the consent, the content of the <template> is put in the dom and placeholders are removed.

## Requirements

This module require [Orejime](https://github.com/empreinte-digitale/orejime) to be installed and running.

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Once the module is enabled, go to your text filters and enable the **Convert Videos into Orejime Videos** filter.

### If you plan using the oEmbed widget on media

If you want to use the oEmbed widget on media, go to your medias display and change the widget of oEmbed field to `Orejime oEmbed content`

**Warning** if you are using orejime video text filter, check that textfilter do not run on media that are using the custom widget.
If it happens, simply put the media text processor after the oreijme video text processor.


### Configuring the filtered domains

The module doesn’t provide, for now, configuration in the admin.
You will need to edit the orejime_videos.settings.yml in your config file and reimport it.

Inside the `filtered_domains` you can setup a list of custom consent (by default we provide examples for youtube, vimeo and twitter).
Example:
```yml
  youtube:
    orejime_consent: youtube
    domains:
      - youtube.com
      - youtu.be
    htmlToExtUrl:
      -
        pattern: '/<iframe[^>]*src\s*=\s*"(?:https?(?::|%3A)\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/(?:watch|embed)?(?:.*v=|.*v%3D|v\/|\/)([\w\-_]+)\&?/im'
        replacement: 'https://youtu.be/$1'
```
For each consent you can define:
- `orejime_consent`: the `name` of the consent in orejime.

  _You can use the same orejime consent name for multiples filters_
- `domains`: the domains names of the service you want to filter, this need to be an array
- `htmlToExtUrl`: a set of rules to replace the "embed" code into the orejime video placeholder.

  _Please note that this field allow multiples entries if needed._
  - `pattern`: a regexp pattern for the replacement.
    Use capturing groups to get elements of the embed that can be used to generate url replacement
  - `replacement`: embed is also replaced by an external url so the user can watch the video outside the website.
    That external url use the capturing group values.

### Translate displayed message

Message displayed can be translated in Drupal's translate interface

### Change displayed message

You can change the displayed message by copy pasting the template to your custom theme and edit it.

### Orejime configuration

Please refer to [Orejime documentation](https://github.com/empreinte-digitale/orejime#configuration).

Here is an example of apps configuration for orejime:
```yml
                {
                  name: "youtube",
                  title: "Youtube",
                  cookies: ["matomo_session", "pk_id", "pk_ses", "_pk_ref", "_pk_cvar"],
                  purposes: ["analytics"],
                  default: false
                },
                {
                  name: "vimeo",
                  title: "Vimeo",
                  cookies: ["matomo_session", "pk_id", "pk_ses", "_pk_ref", "_pk_cvar"],
                  purposes: ["analytics"],
                  default: false
                },
                {
                  name: "twitter",
                  title: "Twitter",
                  cookies: ["matomo_session", "pk_id", "pk_ses", "_pk_ref", "_pk_cvar"],
                  purposes: ["analytics"],
                  default: false
                }
```

### Theme adaptation
Since we replace your
You will need to adapt your theme to apply your video styling to the placeholder.
